# Transit positions api

## Setup

```bash
npm install
serverless dynamodb install
serverless offline start --stage test
serverless dynamodb migrate
```

## Run service offline

```bash
serverless offline start --stage test
```

## Usage

You can start tracking a vehicle and query tracking results with the following commands:

### Start tracking

```
POST http://localhost:3000/positions/track
body: {
	"transport_mode": "bus",
	"operator_id": "0018",
	"vehicle_number": "00460"
}
```


### List coordinates with timestamp

```
GET http://localhost:3000/positions?transport_mode=bus&operator_id=0018&vehicle_number=00460&start_date=2019-11-04T12:50:16.864Z&end_date=2019-11-04T16:59:16.864Z
```

Example Result:
```
[
    {
        "lat": 60.215347,
        "long": 24.739885,
        "date": "2019-11-04T15:07:23.251Z"
    },
    {
        "lat": 60.215439,
        "long": 24.739978,
        "date": "2019-11-04T15:07:24.251Z"
    },
    {
        "lat": 60.215525,
        "long": 24.740077,
        "date": "2019-11-04T15:07:25.251Z"
    },
]
```
