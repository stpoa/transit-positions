const success = ({ statusCode = 200, body = {} }) => ({
  statusCode,
  body: JSON.stringify(body)
});

const failure = ({ statusCode = 500, body = "Internal server errror" }) => ({
  statusCode,
  body: JSON.stringify(body)
});

module.exports = { success, failure }
