"use strict";

const { db, TABLE_NAME } = require("./dynamodb");
const { parseISO } = require("date-fns");

const { success, failure } = require("./api-response.js");

const isInDateRange = ({ startDate, endDate }) => item =>
  parseISO(item.date) >= parseISO(startDate) &&
  parseISO(item.date) <= parseISO(endDate);

const compareItemDates = (a, b) => new Date(a.date) - new Date(b.date);

module.exports.list = async event => {
  const params = event.queryStringParameters;
  // TODO: Validate params
  const transportMode = params.transport_mode;
  const operatorId = params.operator_id;
  const vehicleNumber = params.vehicle_number;
  const startDate = params.start_date;
  const endDate = params.end_date;

  try {
    const result = await db.scan({ TableName: TABLE_NAME }).promise();
    // TODO: these filters should be replaced with proper query to DB
    const items = result.Items.filter(isInDateRange({ startDate, endDate }))
      .filter(item => item.transport === transportMode)
      .filter(item => item.operator === operatorId)
      .filter(item => item.vehicle === vehicleNumber)
      .sort(compareItemDates);

    return success({
      statusCode: 200,
      body: items.map(item => ({
        lat: item.lat,
        long: item.long,
        date: item.date
      }))
    });
  } catch (e) {
    console.error(e);

    return failure({ statusCode: 500, body: "Couldn't fetch data" });
  }
};
