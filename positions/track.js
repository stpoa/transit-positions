"use strict";

const { success, failure } = require("./api-response.js");
const { db, TABLE_NAME } = require("./dynamodb");

const mqtt = require("mqtt");
const client = mqtt.connect("mqtts://mqtt.hsl.fi:8883");

const buildTopic = ({ transportMode, operatorId, vehicleNumber }) =>
  `/hfp/v2/journey/ongoing/vp/${transportMode}/${operatorId}/${vehicleNumber}/#`;

module.exports.track = async event => {
  // TODO: Validate params
  const body = JSON.parse(event.body)
  const transportMode = body.transport_mode;
  const operatorId = body.operator_id;
  const vehicleNumber = body.vehicle_number;

  console.log({ transportMode, operatorId, vehicleNumber });

  const topicFilter = buildTopic({ transportMode, operatorId, vehicleNumber });
  console.log({ topicFilter })

  client.on("connect", function() {
    client.subscribe(topicFilter, function(err) {});

    // Diconnect after 10 minutes
    setTimeout(() => {
      console.log("Disconnecting from API")
      client.end();
    }, 10 * 60 * 1000);
  });

  client.on("message", async function(realTopic, message) {
    console.log('New message')
    const rawItem = JSON.parse(message.toString()).VP;
    const item = {
      topic: realTopic,
      date: rawItem.tst,
      transport: transportMode,
      operator: operatorId,
      vehicle: vehicleNumber,
      lat: rawItem.lat,
      long: rawItem.long
    };

    try {
      await db
        .put({ TableName: TABLE_NAME, Item: item })
        .promise();
      console.log("Item saved to db", realTopic, rawItem.tst);
    } catch (error) {
      console.error(error);
    }
  });

  client.on("error", error => {
    console.error(error);
  });

  return success({});
};
